import 'package:flutter/material.dart';

import 'generated/l10n.dart';
import 'package:intl/intl.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HomeScreenState();
}

class HomeScreenState extends State {
  final _formKey = GlobalKey<FormState>();
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      _counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).home),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text("${S.of(context).language}: "),
              DropdownButton<String>(
                  value: Intl.getCurrentLocale(),
                  icon: const Icon(Icons.arrow_downward),
                  onChanged: (String? newLocale) {
                    setState(() {
                      S.load(Locale(newLocale!));
                    });
                  },
                  items: const [
                    DropdownMenuItem(
                      value: 'en_US',
                      child: Text("English"),
                    ),
                    DropdownMenuItem(
                      value: 'ru_RU',
                      child: Text("Русский"),
                    )
                  ]),
            ]),
            const Spacer(),
            Text(
              S.of(context).counterValue,
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
            const Spacer(),
            Container(
              padding: const EdgeInsets.all(30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: _incrementCounter,
                      child: const Icon(Icons.add)),
                  const Spacer(),
                  ElevatedButton(
                      onPressed: _decrementCounter,
                      child: const Icon(Icons.remove))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
